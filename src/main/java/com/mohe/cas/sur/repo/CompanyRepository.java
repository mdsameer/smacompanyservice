package com.mohe.cas.sur.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mohe.cas.sur.entity.Company;

public interface CompanyRepository extends JpaRepository<Company, Long> {

}
