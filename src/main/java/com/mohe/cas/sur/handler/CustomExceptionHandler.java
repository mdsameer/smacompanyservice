package com.mohe.cas.sur.handler;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.mohe.cas.sur.exception.CompanyNotFoundException;
import com.mohe.cas.sur.payload.response.ErrorMesage;
@RestControllerAdvice
public class CustomExceptionHandler {
	/**
	 * If CompanyNotFoundException is thrown from any RestController then below
	 * method is executed and Returns Error Message with 500 Status code. It is like
	 * a Reusable Catch block code.
	 */
	@ExceptionHandler(CompanyNotFoundException.class)
	public ResponseEntity<ErrorMesage> handleCompanyNotFoundException(CompanyNotFoundException cnfe) {
		return ResponseEntity.internalServerError().body(new ErrorMesage(new Date().toString(), cnfe.getMessage(),
				HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.name()));
	}
}
