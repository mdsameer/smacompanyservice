package com.mohe.cas.sur.service;

import java.util.List;

import com.mohe.cas.sur.entity.Company;



public interface ICompanyService {
	Long createCompany(Company cob);
	void updateCompany(Company cob);
	Company getOneCompany(Long id);
	List<Company> getAllCompanies();


}
